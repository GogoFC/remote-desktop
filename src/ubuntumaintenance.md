# Ubuntu maintenance

### Package managment

Update the package manager cache.

```sh
sudo apt update
```

Upgrade all packages

```sh
sudo apt upgrade
```

Upgrade a specific package

```sh
sudo apt upgrade mpv
```

### Boot messages and error messages

The dmesg utility displays	the contents of	the system message buffer.

```sh
sudo dmesg
```

Systemd has its own logging system called the journal.

Show all messages since after the machine booted.

```sh
journalctl -b
```

