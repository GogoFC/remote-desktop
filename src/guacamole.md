# Guacamole

## Guacamole docker-compose by Boschkundendienst

[Boschkundendienst GitHub](https://github.com/boschkundendienst/guacamole-docker-compose)

Clone the GIT repository:

```sh
git clone "https://github.com/boschkundendienst/guacamole-docker-compose.git"
```
CD into directory, also read the `docker-compose.yml` file, change the database password in two places so it is the same etc.

```sh
cd guacamole-docker-compose
```
Run the `prepare.sh` script.

```sh
./prepare.sh
```
Bring the docker image up in daemon mode with`-d` flag to start guacamole.

```sh
docker compose up -d
```
