# Xrdp 

[Xrdp](http://xrdp.org/) is a Remote Desktop Protocol server.

## Install Xrdp

Install `xrdp` package.

```sh
sudo apt install xrdp 
```

Check xrdp status.

```sh
sudo systemctl status xrdp
```
Add `xrdp` user to `ssl-cert` group.

```sh
sudo adduser xrdp ssl-cert 
```
Restart `xrdp` service. 

```sh
sudo systemctl restart xrdp
```
