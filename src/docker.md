# Docker

Docker is a utility to pack, ship and run any application as a lightweight container.

[Install Docker Engine Documentation](https://docs.docker.com/engine/install/ubuntu/)


## Install Docker Engine on Ubuntu

### Set up the repository.

Update the `apt` package index and install packages to allow `apt` to use a repository over HTTPS:
```sh
sudo apt-get update
```

```sh
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release 
```

Add Docker’s GPG key:

```sh
sudo mkdir -p /etc/apt/keyrings
```
```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```
Set up the repository:

```sh
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null 
```

### Install Docker Engine

Update the `apt` package index:


```sh
sudo apt-get update
```

Install Docker Engine, containerd, and Docker Compose.

```sh
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

### Manage Docker as a non-root user

Add your user to the docker group.

```sh
sudo usermod -aG docker $USER
```

Run the following to activate the changes to groups:

```sh
newgrp docker
```
### Configure Docker to start on boot with systemd

```sh
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```


## Install Docker via Script

[Docker Install Script GitHub](https://github.com/docker/docker-install)

The script automates Docker installation.

```sh
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```
