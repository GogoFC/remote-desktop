# Firewall

## Iptables Firewall

To allow RDP port 3389 in iptables add the following line somwhere below the line allowing port 22.

```sh
-A INPUT -p tcp -m state --state NEW -m tcp --dport 3389 -j ACCEPT
```
Then run the following command to enable the newly added rule.

```sh
iptables-restore < /etc/iptables/rules.v4
```

## Oracle Cloud Firewall

1. Click on the `Instance`.
2. Scroll down and click on the `subnet` under `Primary VNIC` section.
3. Under `Security Lists` click on the `Default Security List for name-of-instance`.
4. Click on `Add Ingress Rules` button.
5. Add a rule.

Add a rule by adding `0.0.0.0/0` to `Source CIDR` box to allow all IP addresses.

![addrule](./images/addrule.png)

6. Click on `Add Ingress Rules` button to save. 

7. The ingress rule should be listed among other rules in the firewall.

![rules](./images/firewallrules.png)
