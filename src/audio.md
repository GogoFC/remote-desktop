# Audio

Original guide at [c-nergy.be](https://c-nergy.be/blog/?p=17734) blog.

## Install prerequisite packages

```sh
sudo apt install build-essential dpkg-dev libpulse-dev git autoconf libtool
```

## Download PulseAudio modules for Xrdp

```sh
cd ~
git clone https://github.com/neutrinolabs/pulseaudio-module-xrdp.git
```

## Execute the Script provided by Xrdp

Change Directory.

```sh
cd ~/pulseaudio-module-xrdp
```
Execute Script.

```sh
scripts/install_pulseaudio_sources_apt_wrapper.sh
```

## Check Script

After running the script check the last line displayed, it will tell you if the script ran successfully or if there are any problems or issues.  This line will also specify where the PULSE_DIR location will be.  

`- All done. Configure PA xrdp module with PULSE_DIR=/home/<%user%>/pulseaudio.src`

## Build the Xrdp pulseaudio module

Build modules by issuing the following commands from the folder `~/pulseaudio-module-xrdp`.

```sh
./bootstrap && ./configure PULSE_DIR=~/pulseaudio.src
```
```sh
make
```

## Install Xrdp

```sh
sudo make install
```

## Confirm

Confirm that the modules have been properly installed by running the following command, and checking if the `module-xrdp-sink.so` and `module-xrdp-source.so` are listed, this means that the PulseAudio modules have been properly built and installed on your system. 

```sh
ls $(pkg-config --variable=modlibexecdir libpulse) | grep xrdp
```
