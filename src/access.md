# Remote Desktop with Ubuntu and Guacamole

Accessing a Remote Desktop Environment on a Ubuntu server using Guacamole and Xrdp.

### Ubuntu connection in Guacamole

![guac](./images/guacamole.png)


### All connections displayed

![guac](./images/guacamole2.png)
