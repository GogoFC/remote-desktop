# Miscelaneous Linux Commands

### Files and Directories


|  Command  |  Description |
| --------- | ------------ |
|`ls`| Lists files.|
|`ls -alh`| Lists all files and their details in human readable format.|
|`cd ~`| Change directory to $HOME directory of current user.|
|`cd ..`| Move one level up.|
|`cd /`| Move to the root directory.|
|`touch note`| Creates an empty file named `note`.|
|`cat note`| Displays contents of file `note`.|
|`cat note > note2`| Copies (overwrites) contents of `note` to `note2`.|
|`cat note >> note2`| Appends contents of `note` to `note2`.|
|`cat note note2 > note3`| Concatenates files `note` and `note2` into file `note3`.|
|`mv note3 note.txt`| Moves and renames file `note3` to file `note.txt`.|
|`rm note.txt`| Removes file `note.txt`.|
|`rm *.txt`| Removes all files ending in `.txt`.|
|`rm -rf /home/user2/`| Removes the directory and everything inside of it recursively.|
|`clear or Ctrl+L`| Clears the terminal.|
|`mkdir dir`| Creates a new directory `dir`.| 
|`rmdir`| Deletes a directory.|


### User Managment

|  Command  |  Description |
| --------- | ------------ |
| `adduser`	| High level utility for adding new user accounts. |
| `deluser`	| Delete a user account. |
| `usermod`	| Modify a user account. |
| `groupadd` | Create a new group. |
| `delgroup` | Delete a group. |


### Misc

|  Command  |  Description |
| --------- | ------------ |
| `hostname` | Displays the name of the host machine. |
| `sudo` | Run a command with superuser privileges. |
| `date` | Displays the current date and time. |
| `cal` | Shows the calendar of the current month. |
| `whoami` | Displays the user name. |
| `who am i` | Displays username and IP from where the user logged in. |
| `chmod +x script.sh` | Adds the executable ability to the file `script.sh`. |
| `env` | Displays all environmental variables. |
| `ip a` | Display NIC information. |
| `reboot` | Reboots the system. |
| `dig quad9.com` | Display DNS information of any domain. |
| `pwd` | Print working directory. |
| `file` | View the type of any file. |
| `find / -name blue` | Find a file named `blue` at root level. |
|  history \| grep ssh | Displays all history containing word 'ssh'. |


## Sudo without password

Using `sudo` command without having to type in the password each time when running a command.

Edit the config file by typing.
```
sudo visudo
```
Find the line

```sh
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL
```
Add `NOPASSWD` on the line. 

Edit the file by placing the cursor after brackets, then type the letter `i` (insert) to enter interactive/editing mode, after typing the word press `esc` to go back into non editing mode. 

Press `shift + :` to go into "command" mode, type `wq!` (write, quit) to save the file and exit.

Visudo uses 'vi' editor, these are 'vi' commands. 

```sh
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) NOPASSWD: ALL
```
Save the file
## Network stats

Install the package

```sh
sudo apt install net-tools
```

Display services running on the network and the port numbers etc..

```sh
sudo netstat -tplnu
```


## Cheat sheets

---

[Linuxconfig commands cheat sheet](https://linuxconfig.org/linux-commands-cheat-sheet)

[Pcwdld commands cheat sheet](https://www.pcwdld.com/linux-commands-cheat-sheet)

[Interviewbit commands cheet sheet](https://www.interviewbit.com/linux-commands-cheat-sheet/)











